'use strict';

// Declare app level module which depends on filters, and services

angular.module('myApp', [
  'myApp.controllers',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'ngAnimate',
  'angularModalService',
  'ngSglclick',
  'ui.router',
  'ngclipboard'
]).

config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state("home", {
            url: "/",
            templateUrl: "partials/home.ejs"
        })
        .state("configuration", {
            url:"/",
            templateUrl: "partials/config.ejs",
            controller: "ConfigCtrl"
        })
        .state("operator", {
            url:"/",
            templateUrl: "partials/operator.ejs",
            controller: "OperatorCtrl"
        })

        .state("management", {
            url:"/",
            templateUrl: "partials/management.ejs",
            controller: "ManagementCtrl"
        })
    ;
}).

config(["$locationProvider", function($locationProvider) {
    $locationProvider.html5Mode(true);
}]).

factory('socket', ['$rootScope', function($rootScope) {
  var socket = io.connect('https://trbots.univ-reims.fr:443');

  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
}]);


