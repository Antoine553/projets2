

var theta = 0;
function offset(el) {
  var rect = el.getBoundingClientRect(),
  scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
  scrollTop = window.pageYOffset || document.documentElement.scrollTop;
  return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

document.getElementById('map').onclick = function(e) {
  var x;
  var y;
  var imgOffset = offset(document.getElementById('map'));
  document.getElementById('p3dx').style.left = e.pageX - 5;
  document.getElementById('p3dx').style.top = e.pageY - 5;
  document.getElementById('p3dx').style.display = 'block';  
  document.getElementById("p3dx").style.transform = "rotate("+theta+"rad)";
  x = e.pageX - imgOffset.top;
  y = document.getElementById('map').height - (e.pageY - imgOffset.left);
  console.log("x = " + x + " y = " + y);
  //pose.x = cell_x*resolution
  //0.227, -2.062
  poseX = ((x * 0.050000) - 14.8).toFixed(3);
  poseY = ((y * 0.050000) - 14.8).toFixed(3);
  console.log("poseX = " + poseX + " poseY = " + poseY);

};

document.getElementById('theta').oninput = function(e) {
  //theta = 2 * Math.asin(-0.20404085179);
  theta = document.getElementById('theta').value;
  var orientationZ = Math.sin(theta / 2).toFixed(3);
  var orientationW = Math.cos(theta / 2).toFixed(3);
  console.log("Orientation Z = "+orientationZ + "    Orientation W = "+orientationW);
  document.getElementById("p3dx").style.transform = "rotate("+theta+"rad)";
};
