function initializeJS() {

    //tool tips
    jQuery('.tooltips').tooltip();

    //popovers
    jQuery('.popovers').popover();

    //custom scrollbar
        //for html
    //jQuery("html").niceScroll({styler:"fb",cursorcolor:"#007AFF", cursorwidth: '6', cursorborderradius: '10px', background: '#F7F7F7', cursorborder: '', zindex: '1000'});
        //for sidebar
    //jQuery("#sidebar").niceScroll({styler:"fb",cursorcolor:"#007AFF", cursorwidth: '3', cursorborderradius: '10px', background: '#F7F7F7', cursorborder: ''});
        // for scroll panel
    //jQuery(".scroll-panel").niceScroll({styler:"fb",cursorcolor:"#007AFF", cursorwidth: '3', cursorborderradius: '10px', background: '#F7F7F7', cursorborder: ''});
    
    //sidebar dropdown menu
    jQuery('#sidebar .sub-menu > a').click(function () {
        var last = jQuery('.sub-menu.open', jQuery('#sidebar'));        
        jQuery('.menu-arrow').removeClass('arrow_carrot-right');
        jQuery('.sub', last).slideUp(200);
        var sub = jQuery(this).next();
        if (sub.is(":visible")) {
            jQuery('.menu-arrow').addClass('arrow_carrot-right');            
            sub.slideUp(200);
        } else {
            jQuery('.menu-arrow').addClass('arrow_carrot-down');            
            sub.slideDown(200);
        }
        var o = (jQuery(this).offset());
        diff = 200 - o.top;
        if(diff>0)
            jQuery("#sidebar").scrollTo("-="+Math.abs(diff),500);
        else
            jQuery("#sidebar").scrollTo("+="+Math.abs(diff),500);
    });

    // sidebar menu toggle
    jQuery(function() {
        function responsiveView() {
            var wSize = jQuery(window).width();
            if (wSize <= 768) {
                jQuery('#container').addClass('sidebar-close');
                jQuery('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                jQuery('#container').removeClass('sidebar-close');
                jQuery('#sidebar > ul').show();
            }
        }
        jQuery(window).on('load', responsiveView);
        jQuery(window).on('resize', responsiveView);
    });

    jQuery('.toggle-nav').click(function () {
        if (jQuery('#sidebar > ul').is(":visible") === true) {
            jQuery('#main-content').css({
                'margin-left': '0px'
            });
            jQuery('#sidebar').css({
                'margin-left': '-180px'
            });
            jQuery('#sidebar > ul').hide();
            jQuery("#container").addClass("sidebar-closed");
        } else {
            jQuery('#main-content').css({
                'margin-left': '180px'
            });
            jQuery('#sidebar > ul').show();
            jQuery('#sidebar').css({
                'margin-left': '0'
            });
            jQuery("#container").removeClass("sidebar-closed");
        }
    });

    //bar chart
    if (jQuery(".custom-custom-bar-chart")) {
        jQuery(".bar").each(function () {
            var i = jQuery(this).find(".value").html();
            jQuery(this).find(".value").html("");
            jQuery(this).find(".value").animate({
                height: i
            }, 2000)
        })
    }

}

jQuery(document).ready(function(){
    initializeJS();
});


function selectupdate(clicked_value){
    var robotName = clicked_value[0]['selectedOptions'][0]['label'];
    clicked_value.parent().attr('data-robotid', robotName);
}

var initphase = 1;
var drake = dragula({
    copy: function (el, source) {
        return source === document.getElementById('left-copy-1tomany');
    },
    accepts: function (el, target) {
        return target !== document.getElementById('left-copy-1tomany');
    },
    invalid: (el, handle) => el.classList.contains('panel-container')
});
drake.containers.push(document.getElementById('left-copy-1tomany'));
drake.on('drag', function(element, container) {
    $('body').addClass('stop-scrolling')
    $('body').bind('touchmove', function(e){e.preventDefault()})
});
drake.on('drop', function(element, container) {
    miseenforme();
    $('body').removeClass('stop-scrolling')
    $('body').unbind('touchmove')
});

function addPhase(){
    if (initphase == 1) {
        $(".lioperator").each(function() {
            var robotName = $(this)[0].lastChild.previousSibling['selectedOptions'][0]['label'];
            $(this).attr('data-robotid', robotName);
        });
    }

    $(".parent .scenarioLayout").append('<div class="phaseope" id="phase-'+initphase+'"><div class="panel-container panel-heading "><span class="panel-title panel-container">'+
    'Phase '+initphase+'</span><div class="panel-actions"><button onclick="removePhase($(this).parent().parent().parent()[0].id)" class="btn-launcher"><i class="fa fa-times iscen"></i></button></div></div><div id="right-copy-'+initphase+'tomany" class="scenarioDiv ng-scope panel panel-info"></div></div>');

    drake.containers.push(document.getElementById('left-copy-1tomany'));
    drake.containers.push(document.getElementById('right-copy-'+initphase+'tomany'));

    initphase = initphase+1;

    $(".phaseope").each(function() {
        var phaseIndex = $(this).index()+1;
        $(this).children(":first").children(":first").html("Phase "+phaseIndex);
    });
    resizePhase()
}

function resizePhase(){
    var maxheight = 0;
    $(".examples .scenarioDiv").each(function() {
        if ($(this).height() > maxheight) {
            maxheight = $(this).height();
        }
    });
    $(".examples .scenarioDiv").each(function() {
        if ($(this).height() < maxheight) {
            $(this).attr("style", "height:"+maxheight+"px;");
        }
    });
}

function clearScenario(){
    $(".parent .scenarioLayout").empty()
    drake.containers.length = 0;
    initphase = 1;
    angular.element('#misejour').triggerHandler('click');
}

function removePhase(phaseid){
    $("#"+phaseid).remove()
    $(".phaseope").each(function() {
        var phaseIndex = $(this).index()+1;
        $(this).children(":first").children(":first").html("Phase "+phaseIndex);
    });
    angular.element('#misejour').triggerHandler('click');
}

function scenmiseenforme(){
    $(".parent .scenarioLayout li").each(function() {
        if ($(this).has("select")) {
            $(this).children().remove();
            $(this).removeAttr("ng-dblclick")
            $(this).attr("ng-class","testAvailability(models.scenario.name, task)");
            $(this).attr("data-popover-trigger","mouseenter");
            $(this).attr("class","ng-scope task_unavailable");
            $(this).append("<span class='ng-binding'>"+$(this).data("taskid")+" - "+$(this).data("robotid")+"</span>");
            $(this).children("#text").remove();
            $(this).attr("onclick", "selectTask($(this))");
        }
    });
}

function miseenforme(){
    scenmiseenforme()
    angular.element('#misejour').triggerHandler('click');

    var maxheight = 0;
    $(".scenarioDiv").each(function() {
        if ($(this).height() > maxheight) {
            maxheight = $(this).height();
        }
    });
    $(".scenarioDiv").each(function() {
        if ($(this).height() < maxheight) {
            $(this).attr("style", "height:"+maxheight+"px;");
        }
    });
};


function selectTask(task){
    $(".parent .scenarioLayout li").each(function() {
        if ($(this).attr('id') != task.attr('id') || $(this).attr('data-robotid') != task.attr('data-robotid') || $(this).parent().attr('id') != task.parent().attr('id')){
            $(this).removeClass('selected')
        }
    })
    if (task.hasClass('selected')) {
        task.removeClass('selected')
    }else{
        task.addClass('selected')
    }
}

function clearTask(){
    $(".parent .scenarioLayout li").each(function() {
        if ($(this).hasClass('selected')){
            $(this).remove()
        }
    })
    miseenforme()
}