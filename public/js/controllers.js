'use strict';
/* Controllers */

angular.module('myApp.controllers', ['ngMaterial', 'ngMessages', 'ngSanitize']).
/*  controller('AppCtrl', function ($scope, $http) {

		$http({
			method: 'GET',
			url: '/api/name'
		}).
		success(function (data, status, headers, config) {
			$scope.name = data.name;
		}).
		error(function (data, status, headers, config) {
			$scope.name = 'Error!';
		});

	}).*/
	 controller('headerMenu', function($scope,$http) {
		$scope.logout = function() {
			$http.get("https://localhost:4443/api/logout?username="+$scope.username+"&apikey="+$scope.apikey).then(function(res){
				window.location = '/';
			})
		};
	}).


	controller('ButtonController', function ($scope, socket, $timeout, ModalService) {

		$scope.reset = function(robot) {
				$timeout(function(){
					if (robot.toggle == 1) {
						socket.emit("stop", {"target" : robot.name, "data": robot.name});
						robot.toggle = 3;
					}
				}, 10000);
		};

		$scope.changeStatus = function (robot) {

				var message = ""
				var type = ""

				////console.log(robot.os);
				if (robot.toggle == 2) {
					message = "Stopping "+robot.name+"...";
					type = "Information";
					if (robot.os == "sim") {
						robot.toggle = 0
						Object.keys(robot.skills).forEach(function(item){
								robot.skills[item] = false; //disbale capacities
								//check for associated sensors
								var pos = $scope.data.sensors.map(function(e) { return e.robot; }).indexOf(robot.name);
								if (pos != -1) {
									delete robot.skills[item] ;
									$scope.data.sensors.splice(pos,1);     
								}
							})
					}
					else {
						socket.emit("stop", {"target" : robot.name, "data":robot.name});
						robot.toggle = 3 ;
					}
				}
				else {
					message = "Starting "+robot.name+"...";
					type = "Request";
					if (robot.os == "sim") {
						
/*              ModalService.showModal({
									template: '<div class="modal fade" tabindex="0"><div class="modal-dialog modal-dialog-alert"><div class="modal-content"><div class="modal-header modal-header-alert">\
								<button type="button" class="close" ng-click="close(\'Cancel\')" data-dismiss="modal" aria-hidden="true">&times;</button>\
								<h4 class="modal-title">Resilience alert</h4>\
							</div>\
							<div class="modal-body">\
							<img src="/img/danger.png"/><p>Problem with <b>Ridgeback</b>:<br>&nbsp;&nbsp;&nbsp; - Can\'t execute "<b>goToUr</b>".<br>&nbsp;&nbsp;&nbsp; - Capacity "<b>mobile</b>" malfunctioning .<br><br> Switch proposal : <b>Pionner</b><br><br>\
							Operator choice: &nbsp;&nbsp;<select><option value ="pioneer" selected>Pioneer</option></select>\
							</p>\
							</div>\
							<div class="modal-footer modal-footer-alert">\
								 <button type="button" ng-click="close(\'No\')" class="btn btn-default" data-dismiss="modal">Abort</button><button type="button" ng-click="close(\'Yes\')" class="btn btn-primary" data-dismiss="modal">Yes</button>\
							</div></div></div></div>',
									controller: "ModalController"
							}).then(function(modal) {
									modal.element.modal({
										backdrop: 'static',
										keyboard: false
									});
									modal.close.then(function(result) {
										var msg_send = result=="Yes" ? "" : "no";
										socket.emit("panne", msg_send) ;
									});
							});  */

						robot.toggle = 2;
						Object.keys(robot.skills).forEach(function(item){
								robot.skills[item] = true;
								////console.log(robot.skills[item]);
							});
						socket.emit("test", {"target" : robot.name, "data": robot.name});
					}
					else {
						socket.emit("start", {"target" : robot.name, "data": robot.name});
						robot.toggle = 1 ;
					}
				}

				$scope.data.messages.push({
					from: 'Me',
					to: 'TalkRobots MW',
					data: {
						type: type,
						value: message
					}
				});
		};
	}).

	controller('Controller', function($scope, ModalService, socket, message) {
		$scope.show = function(rob) {
			for (var i = 0; i < $scope.data.robots.length; i++) {
				if ($scope.data.robots[i].name == rob) {
					var robact = $scope.data.robots[i]
				}
			}
			console.log(robact)
			socket.emit("router",message.createMessage("request","modalOpen", robact.name));
			ModalService.showModal({
					templateUrl: 'modal.html',
					controller: "ModalController"
			}).then(function(modal) {
					modal.element.modal({
						backdrop: 'static',
						keyboard: false
					});
					$("#modal-robot-head").text(robact.name+" Controller")
					if (robact.skills.fly == false || robact.skills.mobile == false) {
						$("#modal-fleche").show()
					}else{
						$("#dronegps").hide()
						$("#modal-fleche").hide()
					}
					if (robact.skills.fly == false) {
						$("#dronegps").show()
						$("#dronevid").show()
					}else{
						$("#dronevid").hide()
					}
					modal.close.then(function(result) {
							socket.emit("router",message.createMessage("request","modalClose", "pioneer"));
					});
			});
		};

	}).

	controller('ModalController', function($scope, close, socket, message) {

		var fired = false;
		$scope.condition_up;
		$scope.condition_left;
		$scope.condition_right;
		$scope.condition_down;
		$scope.condition_3dup;
		$scope.condition_3ddown;

		$scope.offset = function(el) {
			var rect = el.getBoundingClientRect(),
			scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
			scrollTop = window.pageYOffset || document.documentElement.scrollTop;
			return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
		}

		$scope.onclick = function(e) {
			//console.log("test clic image");
			var x;
			var y;
			var imgOffset = $scope.offset(document.getElementById('map'));

			var $div = $(e.target);
			var img = document.getElementById('map');
			var canvas = document.createElement('canvas');
			canvas.width = img.width;
			canvas.height = img.height;
			canvas.getContext('2d').drawImage(img, 0, 0, img.width, img.height);

			var offset = $div.offset();
			var x = e.clientX - offset.left;
			var y = e.clientY - offset.top;
			//console.log("x = " + x + " y = " + y);
			var pixelData = canvas.getContext('2d').getImageData(x, y, 1, 1).data;
			 //console.log(pixelData);
			if(pixelData[0]==255 || pixelData[1]==255 || pixelData[2]==255){
				document.getElementById('p3dx').style.left = x +  e.target.offsetLeft -5 +'px';
				document.getElementById('p3dx').style.top =  y + e.target.offsetTop -5 +'px';
				document.getElementById('p3dx').style.display = 'block';  
				document.getElementById("p3dx").style.transform = "rotate("+$scope.theta+"rad)";

				y  = document.getElementById('map').height - y;
				
				//pose.x = cell_x*resolution
				//0.227, -2.062
				//console.log("x = " + x + " y = " + y);
				var poseX = ((x * 0.050000) - 14.8).toFixed(3);
				var poseY = ((y * 0.050000) - 14.8).toFixed(3);
				//console.log("poseX = " + poseX + " poseY = " + poseY);
			}
		};

		$scope.oninput = function(e) {
			//theta = 2 * Math.asin(-0.20404085179);
			//$scope.theta = document.getElementById('theta').value;
			//console.log($scope.theta);
			var orientationZ = Math.sin($scope.theta / 2).toFixed(3);
			var orientationW = Math.cos($scope.theta / 2).toFixed(3);
			//console.log("Orientation Z = "+orientationZ + "    Orientation W = "+orientationW);
			document.getElementById("p3dx").style.transform = "rotate("+$scope.theta+"rad)";
		};


		socket.on("modal", function(data) {
			//console.log(data.split(':'));
			[$scope.vectx,$scope.vecty,$scope.vectz,$scope.angx,$scope.angy,$scope.angz] = data.split(':');
		});
	
		$scope.close = function(result) {
			close(result, 500); // close, but give 500ms for bootstrap to animate
		 };

		$scope.keydown = function(keyEvent) {
			if (!fired) {
				switch (keyEvent){

					case 38:
						fired = keyEvent;
						$scope.condition_up = true ;
						socket.emit("router",message.createMessage("command","move", "pioneer", [0.1,0,0,0,0,0]));
						break;
					case 37:
						fired = keyEvent;
						$scope.condition_left = true ;
						socket.emit("router",message.createMessage("command","move", "pioneer", [0,0,0,0,0,0.2]));
						break;
					case 39:
						fired = keyEvent;
						$scope.condition_right = true ;
						socket.emit("router",message.createMessage("command","move", "pioneer", [0,0,0,0,0,-0.2]));
						break;
					case 40:
						fired = keyEvent;
						$scope.condition_down = true ;
						socket.emit("router",message.createMessage("command","move", "pioneer", [-0.1,0,0,0,0,0]));
						break;
					case 16:
						fired = keyEvent;
						$scope.condition_3dup = true ;
						socket.emit("router",message.createMessage("command","move", "pioneer", [-0.1,0,0,0,0,0]));
						break;
					case 17:
						fired = keyEvent;
						$scope.condition_3ddown = true ;
						socket.emit("router",message.createMessage("command","move", "pioneer", [-0.1,0,0,0,0,0]));
						break;
				}
			}
		 };

		$scope.keyup = function(keyEvent) {
			if (fired == keyEvent) {
				$scope.condition_up = false ;
				$scope.condition_down = false ;
				$scope.condition_left = false ;
				$scope.condition_right = false ;
				$scope.condition_3dup = false ;
				$scope.condition_3ddown = false ;
				fired = false ;
				socket.emit("router",message.createMessage("command","move", "pioneer", [0,0,0,0,0,0]));
			}
		 };

	}).

	controller('MsgFormController', function ($scope, socket, stateService) {
		$scope.target = null ;
		$scope.type = null;
		$scope.utype = null;
		$scope.content = null;
		$scope.regex = null;

		$scope.change = function() {
				// if ($scope.target) {
				//   $scope.regex = "\\d+";
				// }
		};

		$scope.reset = function() {
			$scope.target = null ;
			$scope.type = null;
			$scope.utype = null;
			$scope.content = null;
			$scope.regex = null;
		};

		$scope.submit = function(isValid) {
				if (isValid) {

					$scope.data.messages.push({
						from: 'Me',
						to: 'ROS-1',
						data: {
							type: 'information',
							value: 'Commande envoyée : launch scenario1.py to ROS-1'
						}
					});
					socket.emit("router", "command:launch:10.9.0.6:scenario1") ;
				}
		};
	}).

	controller('MonitorCtrl', function ($scope, socket, stateService, $filter, ModalService,) {
		socket.emit("userConnect");
		socket.on("userdata", function(data) {
					$scope.username = data.username;
					$scope.apikey = data.apikey;
					$scope.role = data.role;
					$scope.data = stateService.getData($scope.username,$scope.apikey);
					if ($scope.role ==="administrator") {
						$scope.displayUserFormButton = true;
						$scope.canManage = true;
					}
					else{
						$scope.displayUserFormButton = false;
						$scope.canManage = false;
					}
				 
		});


		 //init service saving state
		$scope.newProfile={};
		//console.log($scope.data);

		$scope.emergency_stop = function() {
			var o = null ;
			for (o in $scope.data.robots) {
				if ($scope.data.robots[o].toggle == 2) {
						socket.emit("emergency", {"target" : $scope.data.robots[o].name});
				}
			}
		};

		$scope.classRobot = function (os) {
			var res = "info-box ";
			res += $scope.data.styles[os];
			return res;
		};

		//ack robot start
		socket.on('start/stop', function(robotName) {
				var o = null ;
				for (o in $scope.data.robots) {
					if (($scope.data.robots[o].name).toLowerCase() == robotName) {
						if ($scope.data.robots[o].toggle == 1) {
							$scope.data.robots[o].toggle = 2 ;
							Object.keys($scope.data.robots[o].skills).forEach(function(item){
								$scope.data.robots[o].skills[item] = true;
							});
						}
						else {
							$scope.data.robots[o].toggle = 0 ;
							Object.keys($scope.data.robots[o].skills).forEach(function(item){
								$scope.data.robots[o].skills[item] = false;
								var pos = $scope.data.sensors.map(function(e) { return e.robot; }).indexOf(robotName);
								if (pos != -1) {
									delete $scope.data.robots[o].skills[item] ;
									$scope.data.sensors.splice(pos,1);     
								}
							})
						}
						break ;
					}
				}
		});

		//notify new sensor connection
		socket.on('sensor_register', function(sensor, robotName, val, state) {
				var o = null;
				for (o in $scope.data.robots) {
					if (($scope.data.robots[o].name).toLowerCase() == robotName) {
						// sensor connected
						if (state){
							$scope.data.robots[o].skills[val] = state ;
							$scope.data.sensors.push({
								name: robotName + '_' + sensor,
								robot: robotName,
								type: val,
								value: 0
							});             
						}
						//sensor disconnected
						else {
							delete $scope.data.robots[o].skills[val] ;
							var pos = $scope.data.sensors.map(function(e) { return e.name; }).indexOf(robotName + '_' + sensor);
							$scope.data.sensors.splice(pos,1);     
						}
						break ;
					}
				}
		});

		socket.on('sensor_info', function(type, robotName, sensor, val) {
				var pos = $scope.data.sensors.map(function(e) { return e.name; }).indexOf(robotName + '_' + sensor);
				if (val == "2019") {
					val = "49.287 , 15.24";
				}
				$scope.data.sensors[pos].value = val;
		});

		socket.on('test', function(output) {
			var data = JSON.parse(JSON.stringify(output));
			var options = "";
			for(let i = 0; i < data.candidats.length; i++){
				 options += '<option value ="' + data.candidats[i].toLowerCase() + '"' ;
				 if (data.candidats[i] == data.best) {
						options += " selected" ;
					}
					options += '>' + data.candidats[i] + '</option>' ;
			}
			ModalService.showModal({
									template: '<div class="modal fade" tabindex="0"><div class="modal-dialog modal-dialog-alert"><div class="modal-content"><div class="modal-header modal-header-alert">\
								<button type="button" class="close" ng-click="close(\'Cancel\')" data-dismiss="modal" aria-hidden="true">&times;</button>\
								<h4 class="modal-title">Resilience alert</h4>\
							</div>\
							<div class="modal-body">\
							<img src="/img/danger.png"/><p>Problem with <b>Ridgeback</b>:<br>&nbsp;&nbsp;&nbsp; - Can\'t execute "<b>goToUr</b>".<br>&nbsp;&nbsp;&nbsp; - Capacity "<b>mobile</b>" malfunctionning.<br><br> Switch proposal : <b>' + data.best + '</b><br><br>\
							Operator choice (' + data.candidats.length + ') : &nbsp;&nbsp;<select>' + options +'</select>\
							</p>\
							</div>\
							<div class="modal-footer modal-footer-alert">\
								 <button type="button" ng-click="close(\'No\')" class="btn btn-default" data-dismiss="modal">Abort</button><button type="button" ng-click="close(\'Yes\')" class="btn btn-primary" data-dismiss="modal">Yes</button>\
							</div></div></div></div>',
									controller: "ModalController"
							}).then(function(modal) {
									modal.element.modal({
										backdrop: 'static',
										keyboard: false
									});
									modal.close.then(function(result) {
										var msg_send = result=="Yes" ? "" : "no";
										socket.emit("panne", msg_send) ;
										if (result=="Yes") {
											Object.keys($scope.data.scenarios[0].tasks).forEach(function(phase){
												if (phase > 1) {
													Object.keys($scope.data.scenarios[0].tasks[phase]).forEach(function(task){
														if ($scope.data.scenarios[0].tasks[phase][task].executer == $scope.data.robots[3]) {
															$scope.data.scenarios[0].tasks[phase][task].executer = $scope.data.robots[0];
														}
													})
												}
											})
										}
									});
							});    
		});

		socket.on('resilience', function(sentence){
			//console.log('RESILIENCE');
		// Appending dialog to document.body to cover sidenav in docs app
			ModalService.showModal({
									template: '<div class="modal fade" tabindex="0"><div class="modal-dialog modal-dialog-alert"><div class="modal-content"><div class="modal-header modal-header-alert">\
								<button type="button" class="close" ng-click="close(\'Cancel\')" data-dismiss="modal" aria-hidden="true">&times;</button>\
								<h4 class="modal-title">Resilience alert</h4>\
							</div>\
							<div class="modal-body">\
							<img src="/img/danger.png"/><p>Problem with <b>Ridgeback</b>:<br>&nbsp;&nbsp;&nbsp; - Can\'t execute "<b>goToUr</b>".<br>&nbsp;&nbsp;&nbsp; - Capacity "<b>mobile</b>" malfunctionning.<br><br> Switch proposal : <b>Pionner</b><br><br>\
							Operator choice: &nbsp;&nbsp;<select><option value ="pioneer" selected>Pioneer</option></select>\
							</p>\
							</div>\
							<div class="modal-footer modal-footer-alert">\
								 <button type="button" ng-click="close(\'No\')" class="btn btn-default" data-dismiss="modal">Abort</button><button type="button" ng-click="close(\'Yes\')" class="btn btn-primary" data-dismiss="modal">Yes</button>\
							</div></div></div></div>',
									controller: "ModalController"
							}).then(function(modal) {
									modal.element.modal({
										backdrop: 'static',
										keyboard: false
									});
									modal.close.then(function(result) {
										var msg_send = result=="Yes" ? "" : "no";
										socket.emit("panne", msg_send) ;
										if (result=="Yes") {
											Object.keys($scope.data.scenarios[0].tasks).forEach(function(phase){
												if (phase > 1) {
													Object.keys($scope.data.scenarios[0].tasks[phase]).forEach(function(task){
														if ($scope.data.scenarios[0].tasks[phase][task].executer == $scope.data.robots[3]) {
															$scope.data.scenarios[0].tasks[phase][task].executer = $scope.data.robots[0];
														}
													})
												}
											})
										}
									});
							});    
	});
		
	}).
 
	controller('OperatorCtrl', function ($scope, socket, stateService, message,$http) {

	 
		$scope.models = $scope.data.models;
		$scope.tasks_done = {},
		//availability of each scenario
		$scope.available = [];

		//enable/disable capacity
		$scope.change = function(capacity, newValue) {
			if (newValue) {
				$scope.models.capacities[capacity] += 1 ;
			}
			else {
				$scope.models.capacities[capacity] -= 1 ;
			}
			//refresh availability
			$scope.data.scenarios.forEach(function(scenario, i){
				$scope.available[i] = $scope.testScenario(scenario);
			});
			$scope.available[$scope.available.length-1] = $scope.testScenario($scope.models.scenario)
		};

		//initialize capacities array
		$scope.data.robots.forEach(function(item){
			for (var capacity in item.skills) {
					if (item.skills[capacity]) {
						if (capacity in $scope.models.capacities)
							$scope.models.capacities[capacity] += 1 ;
						else
							$scope.models.capacities[capacity] = 1 ;
					}
			}
		});

			$scope.testAvailability = function(scenarioName, task) {
				var obj = {};
				////console.log($scope.models);
				obj['task_available'] = true ;
				obj['task_unavailable'] = false ;
				obj['task_availableAlt'] = false ; 
				obj['task_done'] = false ;
				obj['selected'] = $scope.models.selected === task ;  
				////console.log(task.name);
				$scope.models.requisits[task.name].forEach(function(requisit){
					//if one requisit is missing on the selected robot, we have to check others
					if (!task.executer.skills[requisit]) {
						obj['task_available'] = false ;
						//if one requisit is missing, task is unavailable
						if (!$scope.models.capacities[requisit] || $scope.models.capacities[requisit] == 0) {
							obj['task_unavailable'] = true ;
						}
						//otherwise, requisit is available on another robot
						else {
							obj['task_availableAlt'] = true ; 
						}
					}
				//  //console.log($scope.tasks_done);
					obj['task_done'] = $scope.tasks_done[scenarioName].includes(task.name);
/*          //console.log("availability: " + scenarioName);
					//console.log($scope.tasks_done[scenarioName]);
					//console.log(task.name);
					//console.log($scope.tasks_done[scenarioName].includes(task.name));
					//console.log(obj['task_done']);*/
				});

				return obj ;
		};

		$scope.testScenario = function(scenario) {
			var valid = true;
			Object.keys(scenario.tasks).forEach(function(phase){
				Object.keys(scenario.tasks[phase]).forEach(function(task){
					if (scenario.tasks[phase][task].name != null && $scope.testAvailability(scenario.name, scenario.tasks[phase][task])['task_available'] == false) {
						valid = false;
					}
				})
			})
			return valid;
		}

		//initialize tasks_done followup
		$scope.data.scenarios.forEach(function(scenario){
				$scope.tasks_done[scenario.name] = [];
		});
		$scope.tasks_done[$scope.models.scenario.name] = [];

		//initialise task/requisits arrays and remove duplicates
		$scope.models.tasksActors=[];
		$scope.data.robots.forEach(function(item){
			Object.keys(item.tasks).forEach(function(key){

					$scope.models.tasks.push(key);
					
					////console.log($scope.models.tasksActors.includes(key));
					//if (!($scope.models.tasksActors.includes(key))) {
					if ($scope.models.tasksActors[key] == undefined) {
						$scope.models.tasksActors[key] = [] ;
						$scope.models.selectedActor[key] = item;
					}
					$scope.models.tasksActors[key].push(item);
					$scope.models.requisits[key] = item.tasks[key];
					////console.log(key,$scope.models.tasksActors[key]);
				});
		});
	
		$scope.models.tasks = $scope.models.tasks.filter(function(elem, index, self) {
			return index == self.indexOf(elem);
		});

		//initialize availability of each scenario
		$scope.data.scenarios.forEach(function(scenario, i){
				$scope.available[i] = $scope.testScenario(scenario);
		});
		$scope.available.push($scope.testScenario($scope.models.scenario));
		////console.log($scope.available);

		$scope.robotOn = function(os) {
			var newArray = $scope.data.robots.filter(function (el) {
				return el.os.toLowerCase() == os ;
			});
			return newArray.some(function(robot){
					return robot.os.toLowerCase() == os && robot.toggle == 2;
			});
		}

		$scope.robotStarted = function() {
			return $scope.data.robots.some(function(robot){
					return robot.toggle == 2;
			});
		}


		$scope.move = function(newIndex) {
			var oldIndex = $scope.models.scenario.indexOf($scope.models.selected);
			if (oldIndex != -1) {
				$scope.models.scenario.splice(newIndex, 0, $scope.models.scenario.splice(oldIndex, 1)[0]);
			}
		}

		$scope.remove = function() {
			//console.log($scope.models.selected);
			var ind = $scope.getIndex($scope.models.selected, $scope.models.scenario.tasks);
			if (ind != null) {
				//enleve la premeire occurence de la tache finie, peut bugger si plusieurs fois la meme dans le scenario
				$scope.tasks_done[$scope.models.scenario.name].splice($scope.tasks_done[$scope.models.scenario.name].indexOf($scope.models.selected.name),1);
				$scope.models.scenario.tasks[ind[0]].splice(ind[1],1) ;
				if ($scope.models.scenario.tasks[ind[0]].length == 0) {
						$scope.models.scenario.tasks.splice(ind[0],1);
						if (ind[0] != 0 && ind[0] < $scope.models.scenario.tasks.length-1) {
							for (var i = 0; i < $scope.models.scenario.tasks[ind[0]].length ; i++) {
								var index = $scope.models.scenario.tasks[ind[0]-1].findIndex(t => t.executer==$scope.models.selectedActor[$scope.models.selected])
								if (index == -1) index = 0 ;
								var sender = $scope.models.scenario.tasks[ind[0]-1][index].executer ;
								$scope.models.scenario.tasks[ind[0]][i].sender = sender ;
							}
						}
						if (ind[0] == 0 && $scope.models.scenario.tasks.length != 0) {
							for (var i = 0; i < $scope.models.scenario.tasks[ind[0]].length ; i++) {
								$scope.models.scenario.tasks[ind[0]][i].sender = null ;
							}
						}
				}
			 // //console.log($scope.models.scenario.tasks);
				$scope.available[$scope.available.length-1] = $scope.testScenario($scope.models.scenario)
			}
		}

		$scope.sendScenario = function(scenario) {
			var m = angular.copy(scenario);
			for(var p = 0; p < m.tasks.length; p++){
				for(var t = 0; t < m.tasks[p].length; t++){
					m.tasks[p][t].executer = m.tasks[p][t].executer.name.toLowerCase() ;
					if (m.tasks[p][t].sender != null) m.tasks[p][t].sender = m.tasks[p][t].sender.name.toLowerCase() ;
				}
			}
			Object.keys(m.tasks[0]).forEach(function(task){
					socket.emit("router",message.createMessage("command","scenario", m.tasks[0][task].executer, m));
				});
		}

		$scope.getIndex = function(task, scenario) {
			if (task != null) {
				for (var phase in scenario){
					for (var t in scenario[phase]) {
						if (scenario[phase][t] == task) {
							return [parseInt(phase), parseInt(t)];
						}
					}
				}      
			}
			return null ;
		}

		$scope.updatename = function(){
					var scenName = $("#scenName").val();
					$scope.models.scenario.name = scenName;
		}

		$scope.copyScenario = function(scenario){
			$scope.models.scenario=JSON.parse(JSON.stringify(scenario));
			$scope.models.scenario.name = "New Scenario";
			var scenLength = $scope.models.scenario.tasks.length;
			for (var i = 0; i < scenLength; i++) {
				addPhase();
				for (var y = 0; y < $scope.models.scenario.tasks[i].length; y++) {
					$('<li id="'+$scope.models.scenario.tasks[i][y].name+'" data-taskid="'+$scope.models.scenario.tasks[i][y].name+'" '+
					' data-robotid="'+$scope.models.scenario.tasks[i][y].executer.name+'" class="lioperator ng-scope" tabindex="0">'+
					'<div class="taskname ng-binding" style="display: contents;">'+$scope.models.scenario.tasks[i][y].name+'</div>'+
					'</li>').appendTo($(".parent .scenarioLayout")[0].lastElementChild.lastElementChild);
				}
			}

			scenmiseenforme()
			$scope.misejour()

		}

		$scope.addDB = function () {
			var scenario = {};
			scenario.name = $scope.newname;
			scenario.tasks = $scope.models.scenario.tasks;
			console.log(scenario);
			$scope.data.scenarios.push(scenario);
			$scope.newname = $('#scenName').val();
			$scope.models.scenario.name = $scope.newname;
			$scope.tasks_done[$scope.models.scenario.name] = [];
			$scope.models.scenario.tasks = [];
			console.log(scenario);
			var req = {
			 method: 'POST',
			 url: "https://localhost:4443/api/addScenario?username="+$scope.username+"&apikey="+$scope.apikey,
			 headers: {
				 'Content-Type': "application/json" 
			 },
			 data: scenario
			}
			$http(req).then(function(res){
				//console.log(res);
			});
		};

		$scope.misejour = function () {
				var scenName = $("#scenName").val();
				var nbPhase = $(".phaseope").length

				$scope.models.scenario = {name:scenName, tasks:[]};

				$(".phaseope").each(function(){
						var tasks = [];
						if ($(this).index() > 0) {
							var num = $(this).index()-1
							var ht = $(this).parent().children()[num].innerText
							ht = ht.substr(ht.indexOf("-") + 2)
							var sender = ht.substr(0,ht.indexOf("\n"))
						}else{
							var sender = null
						}
						$(this).children('.scenarioDiv').children().each(function() {
								tasks.push({'name': $(this).attr("id"), 'sender':sender, 'executer':$scope.models.selectedActor[$(this).attr("id")]})
						})
						$scope.models.scenario.tasks.push(tasks)
				});
				$scope.available[$scope.available.length-1] = $scope.testScenario($scope.models.scenario)
				console.log($scope.models.scenario)
				resizePhase()
		};


		$scope.addTask = function(task) {

			if ($(".phaseope").length != 0) {
				$(".lioperator").each(function() {
					if ($(this).data("taskid") == task) {
						$(this).clone().appendTo($(".parent .scenarioLayout")[0].lastElementChild.lastElementChild);
						scenmiseenforme()
						$scope.misejour()
					}
				});
			}else{
				addPhase()
				$(".lioperator").each(function() {
					if ($(this).data("taskid") == task) {
						$(this).clone().appendTo($(".parent .scenarioLayout")[0].lastElementChild.lastElementChild);
						scenmiseenforme()
						$scope.misejour()
					}
				});
			}
		}

		$scope.removeScenario = function(scenario){
			for(var i in $scope.data.scenarios){
				if(scenario.name === $scope.data.scenarios[i].name){
					$scope.data.scenarios.splice(i,1);
				}
			}
			$http.get("https://localhost:4443/api/removeScenario?username="+$scope.username+"&apikey="+$scope.apikey+"&name="+scenario.name).then(function(res){
				//console.log(res);
			});
		};

		socket.on('task_info', function(message) {
				// message format : scenarioname - phase - taskIndex
				//console.log(message);
				$scope.tasks_done[message.split('-', 3)[0]].push(message.split('-', 3)[2]);
				//console.log(message.split('-', 3)[2]);
				$scope.data.scenarios.forEach(function(scenario, i){
					$scope.available[i] = $scope.testScenario(scenario);
			});
			$scope.available[$scope.available.length-1]=$scope.testScenario($scope.models.scenario);
				$scope.data.messages.push({
					from: 'Me',
					to: 'Web',
					data: {
						type: "statut",
						value: "Done " + message.split('-', 3)[2]
					}
				});

		});
	}).

	controller('ConfigCtrl', function ($scope, $http,stateService) {
		$http.get("https://localhost:4443/api/getRobots?username="+$scope.username+"&apikey="+$scope.apikey).then(function(res){
			$scope.robotList = res.data;

		})
		$scope.showTableRobot = true;
		if ($scope.conf == true)
			$scope.showTableRobot = false;
		if ($scope.script_gen == true)
			$scope.showTableRobot = false;

		$scope.display_form_config = function () {
			$scope.robotsuccess = false;
				$scope.conf = !$scope.conf;
				if ($scope.script_gen) $scope.script_gen = !$scope.script_gen ;
				$scope.showTableRobot = true;
		if ($scope.conf == true)
			$scope.showTableRobot = false;
		if ($scope.script_gen == true)
			$scope.showTableRobot = false;
		}

		$scope.addOS = function () {
				$("#formOS1").removeClass("dn")
				$("#btnOS").removeClass("dc")
				$("#btnOS").addClass("dn")
				$("#btnOS1").removeClass("dn")
				$("#btnOS2").removeClass("dn")
				$("#btnOS1").addClass("dc")
				$("#btnOS2").addClass("dc")
		}

		$scope.clearOS = function () {
				$("#btnOS").removeClass("dn")
				$("#btnOS").addClass("dc")
				$("#btnOS1").addClass("dn")
				$("#btnOS2").addClass("dn")
				$("#formOS1").addClass("dn")
				$("#nomnvOS").val('')
				$("#couleurOS").val('')
		}

		$scope.saveOS = function () {
			$http({
				method: 'POST',
				url: "https://localhost:4443/api/addOS?username="+$scope.username+"&apikey="+$scope.apikey,
				headers: {
					'Content-Type': "application/json" 
				},
				data: {name: $("#nomnvOS").val(), abbr: $("#abbrOS").val(),	type: $("#typeOS").val(), color: $("#couleurOS").val()}
			}).then(function(res){
				//console.log(res)
			});

			$("#repeatSelect").append("<option value='"+$("#abbrOS").val()+"' class='ng-binding ng-scope'>"+$("#abbrOS").val()+"</option>")

			$("#btnOS").removeClass("dn")
			$("#btnOS").addClass("dc")
			$("#btnOS1").addClass("dn")
			$("#btnOS2").addClass("dn")
			$("#formOS1").addClass("dn")
			$("#abbrOS").val('')
			$("#nomnvOS").val('')
			$("#couleurOS").val('')
		}


		$scope.display_script_generator = function () {
			$scope.script_gen = !$scope.script_gen;
			if ($scope.conf) $scope.conf = !$scope.conf ;
				$scope.showTableRobot = true;
			if ($scope.conf == true)
				$scope.showTableRobot = false;
			if ($scope.script_gen == true)
				$scope.showTableRobot = false;
		}

		$scope.reset = function() {
			$scope.robotsuccess = false;
			$scope.newProfile={};
			$scope.data.taskList ={};
		};

		$scope.updateTasks = function () {
			for(var ab in $scope.newProfile.abilities){
				if ($scope.newProfile.abilities[ab]==="") {
					delete $scope.newProfile.abilities[ab];
				}
			}

			if(Object.entries($scope.newProfile.abilities).length != 0 && $scope.newProfile.abilities.constructor === Object){
				var req = {
					 method: 'POST',
					 url: "https://localhost:4443/api/getTasks?username="+$scope.username+"&apikey="+$scope.apikey,
					 headers: { 
						 'Content-Type': "application/json" 
					 },
					 data: $scope.newProfile.abilities
					}
					$http(req).then(function(res){
						$scope.data.taskList = res.data;
					});
			}else{
					$scope.data.taskList ={};
			}
		}

		$scope.submitForm = function() {
			$http({
				method: 'POST',
				url: '/api/script',
				data: $scope.umodel,
				headers: {'Content-Type': 'text/plain'}
			})
			.then(function(data) {
				$scope.scripttext = data.data.script;
			});
		};

		$scope.delete_robot = function(robot) {      
			$http.get("https://localhost:4443/api/deleteRobot?idRobot="+robot+"&username="+$scope.username+"&apikey="+$scope.apikey)
			.then(function(res){
					$http.get("https://localhost:4443/api/getRobots?username="+$scope.username+"&apikey="+$scope.apikey)
					.then(function(result){
						$scope.robotList = result.data;
				});
			});
		}

		$scope.addRobot = function (robots) {
			//console.log(robots);
			robots.taskList = [];
			var select = document.getElementById('taskSelect');
			var options = select && select.options;
			var opt;

			for (var i=0, iLen=options.length; i<iLen; i++) {
				opt = options[i];

				if (opt.selected) {
					robots.taskList.push(opt.value || opt.text);
				}
			}
			//console.log(robots);
			if(robots != undefined){
				var req = {
				 method: 'POST',
				 url: "https://localhost:4443/api/addRobot?username="+$scope.username+"&apikey="+$scope.apikey,
				 headers: { 
					 'Content-Type': "application/json" 
				 },
				 data: robots
				}
				$http(req).then(function(res){
					 if(res.data.affectedRows == 1){
						$scope.robotsuccess = true;
						$scope.newProfile={};
						$scope.data.taskList ={};
						$scope.data = stateService.getData($scope.username,$scope.apikey);
						$http.get("https://localhost:4443/api/getRobots?username="+$scope.username+"&apikey="+$scope.apikey).then(function(res){
							$scope.robotList = res.data;
						})
					}
					else{
						$scope.robotsuccess = false;
					}
					
				});
			}
		}
	}).

	controller('ManagementCtrl', function ($scope, socket, $http) {
		$http.get("https://localhost:4443/api/getUsers?username="+$scope.username+"&apikey="+$scope.apikey).then(function(res){
			$scope.users = res.data;
		})
		if ($scope.role ==="administrator") {
			$scope.displayUserFormButton = true;
			$scope.canManage = true;
		
		}
		else{
			$scope.displayUserFormButton = false;
			$scope.canManage = false;
		}
		$scope.display_user_form = function() {
			$scope.usersuccess = false;
			$scope.conf =false;
			$scope.script_gen = false;
			$scope.displayUserFormButton = false;
			$scope.displayUserForm = true;
		}
		$scope.cancel_form = function() {
			$scope.displayUserFormButton = true;
			$scope.displayUserForm = false;
		}
		$scope.add_user = function(){
			var req = {
				 method: 'POST',
				 url: "https://localhost:4443/api/addUser?username="+$scope.username+"&apikey="+$scope.apikey,
				 headers: { 
					 'Content-Type': "application/json" 
				 },
				 data: $scope.newUser
				}
				$http(req).then(function(res){
					if(res.data.affectedRows == 1){
						$scope.usersuccess = true;
						$scope.newUser={};
					}
					else{
						$scope.usersuccess = false;
					}
					$http.get("https://localhost:4443/api/getUsers?username="+$scope.username+"&apikey="+$scope.apikey).then(function(result){
						$scope.users = result.data;
					})
				});
		}
		$scope.delete_user = function(user){
				$http.get("https://localhost:4443/api/deleteUser?user="+user+"&username="+$scope.username+"&apikey="+$scope.apikey)
				.then(function(res){
					 $http.get("https://localhost:4443/api/getUsers?username="+$scope.username+"&apikey="+$scope.apikey).then(function(result){
						$scope.users = result.data;
					})
					
				});
		}
 });
