'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', []).

factory('message', function() {
    return {
        createMessage: function(type, sstype, target, data = null) {
            if (data) data = JSON.stringify(data);
            return JSON.stringify({
                "type": type,
                "sstype": sstype,
                "target": target,
                "data": data
            });
        }
    };
}).

factory('stateService', function($http) {

    const removeDupliactes = (values) => {
        let concatArray = values.map(eachValue => {
            return Object.values(eachValue).join('');
        })
        let filterValues = values.filter((value, index) => {
            return concatArray.indexOf(concatArray[index]) === index;
        })
        return filterValues;
    }

    var APIData={};
    APIData.getData = function(username , apikey){

    let data = {};
    data.abilities =[];
    data.taskList = [];
    var url = "https://localhost:4443/api/formatedRobotData?username="+username+"&apikey="+apikey;
    $http.get(url).
    then(function(response) {
        ////console.log(response.data);
        data.robots = response.data;
        data.messages = [{
            from: 'Me',
            to: '(555) 251-1234',
            data: {
                type: 'information',
                value: 'test'
            }
        }, ];
        /*	data.os = ['ros', 'abb', 'ur'];
        	data.integration = ['sim', 'modbus'];*/
        data.os = [];
        data.styles=[];
        data.modes= [];
        data.modes.push({"name" : "ROS","mode" : "ros"});
        data.modes.push({"name" : "Interpreter","mode" : "interpreter"});
        data.modes.push({"name" : "Direct","mode" : "direct"});
        data.modes.push({"name" : "None","mode" : " "});
        data.brands=[];
        data.integration = [];
        data.sensors = [];
        data.selected = [];

        for (var item in response.data) {
            ////console.log(item);
            if (response.data[item].type == "os")
                if (!data.os.includes(response.data[item].os)){
                    data.os.push(response.data[item].os);
                    data.selected[response.data[item].os] = null;
                }
            if (response.data[item].type == "integration")
                if (!data.integration.includes(response.data[item].os)){
                    data.integration.push(response.data[item].os);
                    data.selected[response.data[item].os] = null;
                }
        }

        for(var index in data.selected){
            for(var robot in data.robots){
                if (data.robots[robot].os == index) {
                    data.selected[index] = data.robots[robot];
                    break;
                }
            }
            
        }

        data.robots=removeDupliactes(data.robots);
        //console.log(data);
        /*populating data.scenario*/
        $http.get("https://localhost:4443/api/scenarios?username="+username+"&apikey="+apikey).then(function(result) {
            var senarios = result.data;
            var groupedScenarios = [];
            var groupedPhases = [];
            var tasks;
            var fomatedData = [];
            for (var i = 0; i < senarios.length; i++) {
                if (groupedScenarios[senarios[i].name] == undefined) {
                    groupedScenarios[senarios[i].name] = {};
                    groupedScenarios[senarios[i].name].name = senarios[i].name;
                    groupedScenarios[senarios[i].name].tasks = [];
                    tasks = [];
                }
                groupedScenarios[senarios[i].name].tasks.push(senarios[i]);
            }
            var formatedScenario = [];
            for (var scenario in groupedScenarios) {
                formatedScenario.push(groupedScenarios[scenario]);
            }
            for (var scenario in formatedScenario) {
                var executer;
                for (var index in formatedScenario[scenario].tasks) {

                    if (groupedPhases[formatedScenario[scenario].tasks[index].idPhase] == undefined) {
                        groupedPhases[formatedScenario[scenario].tasks[index].idPhase] = {};
                        groupedPhases[formatedScenario[scenario].tasks[index].idPhase].tasks = [];
                        groupedPhases[formatedScenario[scenario].tasks[index].idPhase].name = formatedScenario[scenario].name;
                    }
                    for (var robot in data.robots) {
                        if (data.robots[robot].name == formatedScenario[scenario].tasks[index].robotName) {
                            executer = data.robots[robot];
                            break;
                        }
                    }
                    groupedPhases[formatedScenario[scenario].tasks[index].idPhase].tasks.push({
                        'name': formatedScenario[scenario].tasks[index].taskName,
                        'sender': null,
                        'executer': executer
                    });
                }
                
            }
            ////console.log(groupedPhases);
            groupedScenarios = [];
            for (var index in formatedScenario) {
                if (groupedScenarios[index] == undefined) {
                    groupedScenarios[index] = {};
                    groupedScenarios[index].name = formatedScenario[index].name;
                    groupedScenarios[index].tasks = [];
                }
                for (var phase in groupedPhases) {
                    if (groupedScenarios[index].name == groupedPhases[phase].name) {
                        groupedScenarios[index].tasks.push(groupedPhases[phase].tasks);
                    }
                }
            }
            //saved scenarios are in groupedScenarios
           //setting the sender
            for(var scenario in groupedScenarios){
                for (var phase in groupedScenarios[scenario].tasks){
                    for(var task in groupedScenarios[scenario].tasks[phase])                         
                        if (phase > 0) {
                            groupedScenarios[scenario].tasks[phase][task].sender = groupedScenarios[scenario].tasks[phase-1][Object.entries(groupedScenarios[scenario].tasks[phase-1]).length - 1].executer;
                        
                        }         
                    
                }
                
            }
            //console.log(groupedScenarios);
            data.scenarios = groupedScenarios;
            /*data.scenarios.push( {
                "name": "Demo",
                "tasks": [
                    [{
                        'name': 'conveyor',
                        'sender': null,
                        'executer': data.robots[6]
                    }],

                    [{
                        'name': 'recuperer_piece_table',
                        'sender': null,
                        'executer': data.robots[4]
                    }],

                    [{
                        'name': 'enterToRoom',
                        'sender': data.robots[4],
                        'executer': data.robots[2]
                    }],

                    [{
                            'name': 'takeoff',
                            'sender': data.robots[0],
                            'executer': data.robots[1]
                        },
                        {
                            'name': 'temperature_scenario',
                            'sender': data.robots[0],
                            'executer': data.robots[4]
                        }
                    ],

                    [{
                        'name': 'goAway',
                        'sender': data.robots[4],
                        'executer': data.robots[2]
                    }],

                    [{
                            'name': 'land',
                            'sender': data.robots[0],
                            'executer': data.robots[1]
                        },
                        {
                            'name': 'loadout',
                            'sender': data.robots[0],
                            'executer': data.robots[2]
                        }
                    ]
                ]


            },
            {
                "name": "Demo2",
                "tasks": [
                    [{
                        'name': 'transport',
                        'sender': null,
                        'executer': data.robots[2]
                    }],
                    [{
                        'name': 'conveyor',
                        'sender': null,
                        'executer': data.robots[6]
                    }],
                    [{
                        'name': 'recuperer_piece_table',
                        'sender': null,
                        'executer': data.robots[4]
                    }]
                ]
            });*/
            $http.get("https://localhost:4443/api/getAbilities?username="+username+"&apikey="+apikey).then(function (abilities) {
                data.abilities = abilities.data;
            });
            $http.get("https://localhost:4443/api/getTaskList?username="+username+"&apikey="+apikey).then(function (taskList) {
                data.taskList = taskList.data;
            });
             $http.get("https://localhost:4443/api/getBrand?username="+username+"&apikey="+apikey).then(function (brands) {
                for(var i in brands.data){
                    data.styles[brands.data[i].os] = brands.data[i].color;  
                    data.brands.push(brands.data[i].os);             
                }
            });

    });
        });
        /*end of data.scenario*/
        console.log(data)

        data.models = {
            selected: null,
            tasks: [],
            tasksActors: [],
            selectedActor: [],
            requisits: [],
            capacities: [],
            scenario: {
                "name": "New Scenario",
                "tasks": []
            },
        };
    return data;
    }

    return APIData;
})
