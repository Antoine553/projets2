
/*
 * GET home page.
 */

exports.auth = function(req, res){
  res.render('auth');
};

exports.index = function(req, res){
  res.render('index');
};

exports.partials = function (req, res) {
  var name = req.params.name;
  res.render('partials/' + name);
};