/*
 * Serve JSON to our AngularJS client
 */

exports.name = function (req, res) {
  res.json({
    name: 'Bob'
  });
};

exports.script = function (req, res) {
	console.log(req.body);
	var scripttext  = "s=connect('ip_trbots','port')\nfunction recv_msg():\n\ts.send('ready')\n\tsrc = s.recv()\n\ttype=s.recv()\n\tsstype=s.recv()\n\tsize=s.recv()\n\tdata=s.recv()\n\n//message must be 'type/sstype/target/size/data'\nfunction send_msg(message):\n\ts.send(message)\n\nfunction handle():\n\tif type=='commande':\n\t\tdothis\n\tif type=='request'\n\t\tdothat\n";
	
	if (req.body == 'ROS') {
		scripttext = "scriptROS";
	}
	else if (req.body == 'UR') {
		scripttext = "open=socket_open('ip','port')\nwhile (open == False):\n\topen=socket_open('ip','port')\nend\n\ndef recv_msg():\nsocket_send_string('ready')\nwhile from=='':\n\tfrom = socket_read_string()\nend\ntype=socket_read_ascii_float(1)\nstype=socket_read_string()\nsize=socket_read_ascii_float(1)\ndata=socket_read_string()\nend\n\ndef send_msg(message):\n\tsocket_send_string(message)\nend\n\ndef handle():\nif type=='commande':\n\tdothis\nif type=='request'\n\tdothat\n";
	}
	else if (req.body == 'ABB') {
		scripttext = "scriptABB";
	}
	res.json({
    	script: scripttext
  });
};