
/**
 * Module dependencies
 */

var express = require('express'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  errorHandler = require('express-error-handler'),
  morgan = require('morgan'),
  crypto = require('crypto'),
  routes = require('./routes'),
  api = require('./routes/api'),
  http = require('http'),
  https = require('https'),
  fs = require('fs'),
  exec = require('ssh-exec'),
  session = require('cookie-session'), // Charge le middleware de sessions
  path = require('path'),
  Enum = require('enum'),
  mongodb = require('mongodb').MongoClient,
  urldb = "mongodb://localhost:27017/mydb",
  dgram = require('dgram'),
  mysql = require('mysql'),
  passport = require('passport'),
  fs = require('fs'),
  shell = require('child_process');




var app = module.exports = express();
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

/**
 * Configuration
 */

// all environments
app.set('port', process.env.PORT || 4443);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(bodyParser.text())
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));

var env = process.env.NODE_ENV || 'development';

// development only
if (env === 'development') {
  app.use(errorHandler());
}

// production only
if (env === 'production') {
  // TODO
}


/**
 * Routes
 */

/**
 * Serveur MySQL && REST APIS
 */
let connectedUser , apikey , role;
class Database {
    constructor(  ) {
        this.connection = mysql.createConnection( {
          host: "localhost",
          user: "test",
          password: "test",
          database: "trbotsmodified"
      } );
    }
    query( sql, args ) {
        return new Promise( ( resolve, reject ) => {
            this.connection.query( sql, args, ( err, rows ) => {
                if ( err ){
                    return reject( err );
                }
                resolve( rows );
            } );
        } );
    }
    close() {
        return new Promise( ( resolve, reject ) => {
            this.connection.end( err => {
                if ( err )
                    return reject( err );
                resolve();
            } );
        } );
    }
}

let robots = {};
var dbcon = new Database();
dbcon.query("select LOWER(name) name , ip_address from ROBOT").then(rows => {
  for(var i in rows){
    robots[rows[i].name] = rows[i].ip_address;
  }
});
let apikeys = [];
let userRoles = [];
var dbcon = new Database();
dbcon.query("select username , apikey , role from OPERATOR").then(rows => {
  for(var i in rows){
    apikeys[rows[i].username]=rows[i].apikey;
    userRoles[rows[i].username]=rows[i].role;
  }
});
app.post("/api/addUser" , function(req,res){
if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined) && (userRoles[req.query.username]!="administrator")) {
    res.json({"error" : "not allowed"});
  }
  else {
    var userapikey = "";
    var characters       = 'ABCDEFGHIJKabcdefghijk<0123456789>LMNOPQRSTUVWXYZlmnopqrstuvwxyz';
    var charactersLength = characters.length;
    for ( var i = 0; i < 10; i++ ) {
      userapikey += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    var password = crypto.createHmac('sha512','trobots').update(String(req.body.password)).digest('hex');
    var query = "insert into OPERATOR (username , password , apikey , role) values ('"+req.body.username+"','"+password+"','"+userapikey+"','"+req.body.role+"')"; 
    var con = new Database()
    con.query(query).then(result => {          
      var dbcon = new Database();
      dbcon.query("select username , apikey , role from OPERATOR").then(rows => {
        for(var i in rows){
          apikeys[rows[i].username]=rows[i].apikey;
          userRoles[rows[i].username]=rows[i].role;
        }
         res.json(result);
      });

    });
  }
});

app.get('/api/getBrand', function (req, res) {
  if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
    var query = "SELECT name , LOWER(abbr) os , type ,color from BRAND"; 
    var con = new Database()
    con.query(query).then(rows => {          
      res.json(rows);
    });
  }
});

app.post('/api/getTasks', function (req, res) {
  if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
  var strList ="" ;
  for (var i in req.body ){
    strList += req.body[i] +" ,";
  }
  strList = strList.slice(0, -1);
  var query = "SELECT name , id from TASK where id in(select idTask from TASK_ABILITY where idAbility in ("+strList+"))"; 
  //console.log(query);
  var con = new Database()
  con.query(query).then(rows => {          
    res.json(rows);
  });    
}
});

app.get('/api/formatedRobotData', function (req, res) {
 if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
  let robotsData ;
  let robotsTasks ;
  let robotsAbilities ;
  var robotQuery = "SELECT ROBOT.* , LOWER(BRAND.abbr) os , BRAND.type type from ROBOT inner join BRAND on BRAND.id = ROBOT.idBrand order by ROBOT.idBrand";
      con = new Database();
        con.query(robotQuery).then(rows => {
        robotsData = rows;
        var query = "SELECT TASK.name taskname , ABILITY.name abilityname ,ROBOT_TASK.idRobot id from TASK inner join ABILITY , TASK_ABILITY , ROBOT_TASK where TASK.id = TASK_ABILITY.idTask and TASK.id = ROBOT_TASK.idTask and ABILITY.id = TASK_ABILITY.idAbility" ;
        return con.query(query);
        }).then( rows => {
          robotsTasks = rows;
          var query = "SELECT ABILITY.name abilityname , ROBOT.id id from ROBOT inner join ABILITY , ROBOT_ABILITY where ABILITY.id = ROBOT_ABILITY.idAbility and ROBOT.id = ROBOT_ABILITY.idRobot";
          return con.query (query)
        }).then(rows => {
          robotsAbilities = rows;
          return con.close();
        }).then(() => {
          /*//console.log(robotsAbilities);
          //console.log(robotsTasks);*/
          //grouping tasks and abilities by robots
          let robotTaskList = {},robotAbilitiesList ={};
          for (var i = 0; i < robotsData.length; i++) {
            robotTaskList[i]=[];
            robotAbilitiesList[i]=[];
          }
          for (var i = 0; i < robotsData.length; i++) {
            for (var j = 0; j < robotsTasks.length; j++) {
              if(robotsTasks[j].id == robotsData[i].id)
              {
                robotTaskList[i][robotsTasks[j].taskname] = [];               
              }
            }
            for (var j = 0; j < robotsTasks.length; j++) {
              if(robotsTasks[j].id == robotsData[i].id)
              {
                robotTaskList[i][robotsTasks[j].taskname].push(robotsTasks[j].abilityname);
                ////console.log(robotsTasks[j]);
              }
            }
            for (var j = 0; j < robotsAbilities.length; j++) {
              if(robotsAbilities[j].id == robotsData[i].id)
              {
                robotAbilitiesList[i][robotsAbilities[j].abilityname] = false;
              }
            }
          }      
          var formated_result = [];
          for (var i = 0; i < robotsData.length; i++) {
            
            formated_result.push({
            "name": robotsData[i].name ,
            "id": robotsData[i].id,
            "os" : robotsData[i].os,
            "type" : robotsData[i].type,
            "toggle": 0,
            "skills": Object.assign({}, robotAbilitiesList[i]),
            "tasks": Object.assign({}, robotTaskList[i])
          })
          }
          ////console.log(formated_result);
          res.json(formated_result);
        }).catch( err => {
        //console.log(err);
    });
  }
});

app.get("/api/scenarios", function (req, res) {
  if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
  let scenarios , tasks , phases , robots;
  var con = new Database();
  var query = "SELECT SCENARIO.id id , SCENARIO.name name , PHASE.id idPhase , TASK.name taskName , ROBOT.name robotName ,  TASK.duration FROM SCENARIO , PHASE , TASK_PHASE , TASK , ROBOT where SCENARIO.id = PHASE.idScenario and TASK_PHASE.idPhase = PHASE.id and TASK.id = TASK_PHASE.idTASK and ROBOT.id = TASK_PHASE.idROBOT";
  con.query(query).then( rows => {
    res.json(rows);
  });
}
});
app.get("/api/getAbilities", function (req, res) {
  if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
  let abilities= [];
  var con = new Database();
  var query = "SELECT name , id from ABILITY ";
  con.query(query).then( rows => {
    for(r in rows){
      abilities.push({
        'name' : rows[r].name ,
        'id' : rows[r].id
      }) 
    }
    res.json(abilities);
  });
}
});


app.get("/api/getTaskList", function (req, res) {
  if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
  let tasks= [];
  var con = new Database();
  var query = "SELECT name , id , duration from TASK ";
  con.query(query).then( rows => {
    for(r in rows){
      tasks.push({
        'name' : rows[r].name ,
        'id' : rows[r].id ,
        'duration' : rows[r].duration
      }) 
    }
    res.json(tasks);
  });
}
});



app.post("/api/addScenario" , function(req, ret) {
if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
  var con = new Database();
  var phases = req.body.tasks;
  var query = "INSERT INTO SCENARIO (name,nbPhases) VALUES ('"+req.body.name+"',"+phases.length+")";
  con.query(query).then( res => {
    var phaseQuery = "INSERT into PHASE (idScenario,nbTasks) values ";
    for(var phaseIndex in phases ){
      phaseQuery += "("+res.insertId+","+phases[phaseIndex].length+") ,";
    } 
    phaseQuery = phaseQuery.slice(0, -1);
    var con2 = new Database();
    con2.query(phaseQuery).then(result=>{
      var firstAdded = result.insertId;
      var taskPhaseQuery = "insert into TASK_PHASE (idPhase , idROBOT , idTask) values"
      for(var phaseIndex in phases ){
        for(task in phases[phaseIndex]){
          var idPhase = parseInt(firstAdded)+parseInt(phaseIndex);
          taskPhaseQuery += "("+idPhase+","+phases[phaseIndex][task].executer.id+","+phases[phaseIndex][task].name+") ,";
        }
      } 
      taskPhaseQuery = taskPhaseQuery.slice(0, -1);
      var con3 = new Database();
      var getTasks = "select name , id from TASK";
      con3.query(getTasks).then(r=>{
        //console.log(taskPhaseQuery);
        for(var i in r){
        taskPhaseQuery = taskPhaseQuery.replace(r[i].name , r[i].id);
        } 
        var con4 = new Database();
        con4.query(taskPhaseQuery).then(dbresult => {
        ret.json(dbresult);
        });
      });
    });
  });
}
});

app.get("/api/removeScenario", function(req , res){
 if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
  var con = new Database();
  var query = "delete from SCENARIO where name = '"+req.query.name+"'";
  //console.log(query);
  con.query(query).then(ret => {
    res.json(ret);
  })
}
});

app.post("/api/addOS" , async function(req, res) {
if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
    var con = new Database();
    try {
        const result = await con.query(
          'INSERT INTO BRAND (name, abbr, color, type) VALUES (?, ?, ?, ?)', [req.body.name, req.body.abbr, req.body.color, req.body.type]
        );
        res.status(201).json(result);
      }
      catch (error) {
        res.status(500).json({message: error.message});
      }
    }
});


app.post("/api/addRobot",function(req , res) {
if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
  let robot = req.body
  /*sending the file to the target*/
  var config = {

                      "_comment" : "Listening port for external messages (web or GUI)",

                      "UDP_PORT" : robot.extport,  

                      "_comment" : "Listening port for external messages (robots)",

                      "TCP_PORT_EXT" : 45000, 

                      "_comment" : "Listening port for internal messages (from handled robot)",

                      "TCP_PORT" : robot.intport, 

                      "_comment" : {
                              "Interface used for local network communication": "",
                              "(when VPN is shutdown)": "",
                              "On Windows : usually 'Ethernet' or 'Wi-Fi'": "",
                      "On Linux : usually 'eth0', 'enp3s0' (ethernet) or 'wlan0' (Wifi)": ""
                  },

                  "HOST_INT" : robot.ext, 

                      "_comment" : {
                              "Interface used to communicate trhough VPN" :"",
                              "(with web server for example)":"",
                              "On Windows (OpenVPN) usually 'Ethernet 2'":"",
                              "On Linux : usually 'tun0'":""
                      },

                      "VPN_INT" : "tun0"  ,

                      "SIGNATURE" : 1,

                      "CRYPTO" : 1,

                      "ROBOT_INT" : robot.int
              };
  fs.writeFile('config.json', JSON.stringify(config), function (err) {
    if (err) throw err;
    ////console.log('File is created successfully.');
    shell.exec('scp2 /var/www/TRBots/config.json trbots:trbots@'+robot.ip_address+':/opt/v3trbots/', (err, stdout, stderr) => {
      if (err) {
        return;
      }
      shell.exec('rm -f config.json', (err, stdout, stderr) => {
        if (err) {
          return;
        }
      });
    });
  });             
  /*end of sending file*/
  var request = "select id from BRAND where abbr ='"+robot.os+"'"; 
  var con = new Database();
  let addedId;
  //console.log(robot);
  con.query(request).then(rows => {
    ////console.log(rows);
    return con.query("insert into ROBOT (name, ip_address, idBrand, mode) values ('"+robot.name+"' , '"+robot.ip_address+"' ,"+rows[0].id+",'"+robot.mode+"')");
  }).then(rows => {
 
    addedId = rows.insertId;
    var query = "insert into ROBOT_ABILITY (idROBOT , idAbility) values ";
    for(var ability in robot.abilities ){
      query += "("+addedId+","+robot.abilities[ability]+") ,";
    } 
    query = query.slice(0, -1);
    ////console.log(query);
    return con.query(query)
  }).then(rows =>{
    var sqlquery = "insert into ROBOT_TASK (idRobot,idTask) values";
    for(var i in robot.taskList)
    {
      sqlquery += "("+addedId+","+robot.taskList[i]+") ,";
    }
    sqlquery = sqlquery.slice(0, -1);
    ////console.log(sqlquery);
    return con.query(sqlquery)
  }).then(rows => {
    res.json(rows);
  });
  
}
});

app.get("/api/deleteUser" , function(req ,res){
  if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
    var query = "delete from OPERATOR where username = '"+req.query.user+"'"; 
    var con = new Database()
    con.query(query).then(rows => {          
      res.json(rows);
    });
  }
});

app.get("/api/deleteRobot" , function(req ,res){
  if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
     var query = "delete from ROBOT where id = "+req.query.idRobot; 
    var con = new Database()
    con.query(query).then(rows => {          
      res.json(rows);
    });
  }
});

app.get("/api/getUsers" , function(req ,res){
  if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
    var query = "select * from OPERATOR where username <> '"+req.query.username+"'"; 
    var con = new Database()
    con.query(query).then(rows => {          
      res.json(rows);
    });
  }
});

app.get("/api/getRobots" , function(req ,res){
  if(!(apikeys[req.query.username]===req.query.apikey) || (req.query.username == undefined) || (req.query.apikey == undefined)) {
    res.json({"error" : "not allowed"});
  }
  else {
     var query = "select ROBOT.id , ROBOT.name , ROBOT.ip_address , ROBOT.mode , BRAND.abbr from ROBOT inner join BRAND on ROBOT.idBrand = BRAND.id"; 
    var con = new Database()
    con.query(query).then(rows => {          
      res.json(rows);
    });
  }
});

/*end of REST APIs*/


// serve index and view partials
//app.get('/', routes.index);
app.get('/partials/:name', routes.partials);

// JSON API
app.get('/api/name', api.name);

// redirect all others to the index (HTML5 history)
app.get('*', routes.auth);


app.post('/api/script', api.script);

/* PASSPORT LOCAL AUTHENTICATION */

const LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(
  function(username, password, done) {
    let pwd; 
    var con = new Database();
    con.query("select password , apikey , role from OPERATOR where username ='"+username+"'").then(rows=>{
      if(rows.length == 0) {
        return done(null, false);
      }
      pwd = rows[0].password;

      password = crypto.createHmac('sha512','trobots').update(password).digest('hex');
      //console.log(password);
      if (!username || !password) {
          return done(null, false);
        }

      if (! (password === pwd)) {
          return done(null, false);
        }
      connectedUser = username;
      apikey = rows[0].apikey;
      role = rows[0].role;
      return done(null, username);
    }).catch(err =>{
      //console.log(err);
    });
        
  }));

app.post('/', passport.authenticate('local', { failureRedirect: '/' }),
  function(req, res) {
    //console.log(connectedUser);
    //console.log(apikey);
    res.render('index');
  }
);

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});



/**
 * Start Server
 */
var server = https.createServer({
	key: fs.readFileSync('key_nopwd.pem'),
	cert: fs.readFileSync('cert.pem')
}, app);
// Chargement de socket.io
var io = require('socket.io').listen(server);

//IOSocket server to receive messages from web-user
io.sockets.on('connect', function (client) {
    console.log('Un client est connecté ! ');

    client.on("userConnect", function(data) {
      io.to(client.id).emit("userdata", {"username":connectedUser,"apikey":apikey,"role":role});
    });
});

server.listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
